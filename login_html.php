<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8"> 
    <link rel="stylesheet" type="text/css" href="style.css">  
    <title>Авторизация</title>
</head>
<body class="body_login">
    <section id="login">
        <div class="container">
            <?php echo (isset($addUser)) ? $addUser : "<h1>Авторизация</h1> "; ?>                                  
            <form class="form_login" action="login.php" method="POST">
                <div class="form-group">
                    <label for="lg" class="sr-only">Логин</label> 
                    <input class="login_input" type="text" placeholder="Логин" name="login" id="lg" class="form-control">
                </div>
                <div class="form-group">
                    <label for="key" class="sr-only">Пароль</label>
                    <input class="login_input" type="password"  placeholder="Пароль" name="password" id="key" class="form-control">
                </div>
                <input class="login_input" type="submit" class="btn" value="Войти">
            </form> 
            <h2>Не зарегистрированы? Не беда :) <br> Введите логин и пароль и нажмите зарегистрироваться</h2>     
            <form class="form_login" action="login.php" method="POST">
                <div class="form-group">
                    <label for="new_lg" class="sr-only">Логин</label> 
                    <input class="login_input" type="text" placeholder="Имя" name="new_login" id="new_lg" class="form-control">
                </div> 
                <div class="form-group">
                    <label for="new_key" class="sr-only">Пароль</label>
                    <input class="login_input" type="password"  placeholder="Пароль" name="new_password" id="new_key" class="form-control">
                </div>           
                <input class="login_input" type="submit" class="btn" value="Зарегистрироваться">
            </form>                    
        </div> <!-- /.container -->
    </section>
</body>
</html>