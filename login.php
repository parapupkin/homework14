<?php
    require_once'functions.php';
    // если авторизован - переходим на admin
    if (isAuthorized()) {
        redirect('admin');
    }
   
    // выполняем проверку, правильности введенных логина и пароля
    if (!empty($_POST['login'])) {        
        if (isCorrectUser()) {           
            redirect('admin');          
        } else {
            die("Неверно введен логин или пароль");
        }       
    }
    // выполняем проврку, существует ли такое имя пользователя при попытке регистрации 
    if (isDate()) {
        if (!empty($_POST['new_login']) && !empty($_POST['new_password'])) { 
            if (!isUser()) {
               $addUser = addUser();
            } else {
                die("Пользователь с таким именем уже существует");
            }    
        }        
    }
    // запускаем вывод страницы на экран
    require_once'login_html.php';
?>

