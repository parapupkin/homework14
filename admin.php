<?php
    require_once'functions.php';

    try {
        // если есть запросы по сортировке - выполняем запрос с сортировкой, если нет - выполняем запрос без сортировки

        if (!empty($_GET['selection'])) {
            switch ($_GET['selection']) {
                case 'date':
                    $selection = 'task.date_added';
                    break;
                case 'status':
                    $selection = 'task.is_done';
                    break;                             
                default:
                    $selection = 'task.description';
                    break;
            }                                                      
            $sql = dbConnect()->prepare("SELECT u1.id AS user_id, task.id, description, is_done, date_added, u1.login AS login1, u2.login AS login2  FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id WHERE user_id = :user_id ORDER BY $selection");
            $sql->execute([
                ':user_id' => $_SESSION['user']['id']                    
            ]);
           
        } else {
            $sql = dbConnect()->prepare("SELECT u1.id AS user_id, task.id, description, is_done, date_added, u1.login AS login1, u2.login AS login2  FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id WHERE user_id = :user_id");                
            $sql->execute([
                'user_id' => $_SESSION['user']['id']
            ]); 
        }        

        // запрос для получения списка пользователей (выводится в select)
        $sql_logins = dbConnect()->query("SELECT id, login  FROM `user`");
        $logins = $sql_logins->fetchAll();

        // запрос для вывода списка задач, полученных от других пользователей
        $sql_require = dbConnect()->prepare("SELECT u2.id AS user_id, task.id, description, is_done, date_added, u1.login AS login1, u2.login AS login2  FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id WHERE assigned_user_id = :user_id AND user_id != :user_id");            
        $sql_require->execute([
            'user_id' => $_SESSION['user']['id']
        ]);

        // если отправлен id дела, которое нужно отредактировать, записываем его
        if (!empty($_GET['id'])) {
            $id = $_GET['id'];
        }  
        // по умолчанию поле для вставки новой записи предназначено для вставки, ниже есть возможность изменить его назначение при редактировании записей
        $name_insert = "insert";         
       
        if (isset($_GET['action'])) {
                // удаление записи
            if ($_GET['action'] == 'delete') {               
                $sql_delete = dbConnect()->prepare("DELETE FROM task WHERE `task`.`id` = :id");
                $sql_delete->bindValue(':id', $id, PDO::PARAM_INT);
                $sql_delete->execute();
                redirect('admin');
            }

            // пометка записи, как выполненной
            if ($_GET['action'] == 'done') {                  
                $sql_done = dbConnect()->prepare("UPDATE `task` SET `is_done` = '1' WHERE `task`.`id` = :id");
                $sql_done->bindValue(':id', $id, PDO::PARAM_INT);
                $sql_done->execute();
                redirect('admin');
            }

            // редактирование записи
            if ($_GET['action'] == 'edite') {        
                $sql_edite = dbConnect()->prepare("SELECT * FROM task WHERE `task`.`id` = :id");
                $sql_edite->bindValue(':id', $id, PDO::PARAM_INT);
                $sql_edite->execute();
                $value = $sql_edite->fetch();
                $name_insert = "update";
            }       
        }

        // вставка записи
        if (!empty($_GET['insert'])) {        
            $sql_insert = dbConnect()->prepare("INSERT INTO `task` (user_id, description, date_added, assigned_user_id) VALUES (:user_id, :value, NOW(), :assigned_user_id)");
            $sql_insert->execute([
                ':user_id' => $_SESSION['user']['id'],
                ':value' => $_GET['insert'],
                ':assigned_user_id' => $_SESSION['user']['id']
            ]);
            redirect('admin');
        } 

        // обновление отредактированной записи
        if (!empty($_GET['update'])) {            
            $sql_update = dbConnect()->prepare("UPDATE `task` SET `description` = :description WHERE `task`.`id` = :id");        
            $sql_update->execute([
                ':id' => $id,
                ':description' => $_GET['update']
            ]);        
            redirect('admin');
        }    

        // назначение нового пользователя ответственным за выполнение задачи
        if (isset($_GET['date_id']) && isset($_GET['assign_user'])) {        
                $sql_assign = dbConnect()->prepare("UPDATE `task` SET `assigned_user_id` = :user_id WHERE `task`.`id` = :id");                
                $sql_assign->execute([
                    ':user_id' => $_GET['assign_user'],
                    ':id' => $_GET['date_id']
                ]); 
                redirect('admin');               
            }     

    } catch (Exception $e) {
        die('Error: ' . $e->getMessage() . '<br/>');
    }
    // запускаем вывод страницы на экран
    require_once'admin_html.php';     
        
?>

   