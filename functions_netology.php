<?php
    session_start();
    // Для того чтобы выводить все ошибки и предупреждения
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    // Установка и проверка времени жизни сессии, если пользователь долго не обращался к странице - удаляем сессию и создаем новую
    $lifeLimit = 300;
    if(isset($_SESSION['time'])&&(time()-$_SESSION['time'])>$lifeLimit) {
            session_destroy();
            session_start();
            redirect('index'); 
    }
    $_SESSION['time']=time();

    /**
     * коннектимся к базе данных
     * @return PDO object
     */
    function dbConnect() 
    {
        $host = 'localhost';
        $dbname = 'vustinovich';
        $user = 'vustinovich';
        $pass = 'neto1547';    
        $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $pass);
        return $db;
    }

    function isDate()
    {
        if ((isset($_POST['new_login']) || isset($_POST['new_password'])) && (empty($_POST['new_login']) || empty($_POST['new_password']))) {
            die("Введите все необходимые данные для регистрации");
            return false;
        } else {
            return true;
        }
    }

    /**
     * получаем данные логине вновь регистрируемого пользователя для проверки на существование
     * @return bool
     */
    function isUser()
    {   
        try {          
            $new_login = $_POST['new_login'];
            $sql = dbConnect()->prepare("SELECT `login` FROM `user` WHERE `login` LIKE ?");        
            $sql->execute([$new_login]);
            $user = [];
            $user = $sql->fetch();
            return !empty($user);
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        }  
    } 

    /**
     * добавляем данные логине вновь регистрируемого пользователя
     * @return str
     */
    function addUser() 
    {
        try {          
            $newLogin = $_POST['new_login'];
            $newPass = md5($_POST['new_password']);
            $newUser = [
                ':newLogin' => $newLogin,
                ':newPass' => $newPass
            ];        
            $sql_add = "INSERT INTO `user` (`login`, `password`) VALUES (:newLogin, :newPass)";
            $addUser = dbConnect()->prepare($sql_add)->execute($newUser);
            if ($addUser == 1) {
                return "<h1>Вы успешно зарегистрированы<br>Введите логин и пароль, чтобы войти</h1>";
            } 
            if ($addUser == 0) {
                return "<h1>Ошибка при регистрации</h1>";
            }                  
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        } 
    }

    /**
     * получаем данные о пользователе с введенным логином и паролем
     * @return bool
     */
    function isCorrectUser()
    {
        try { 
            $login = $_POST['login'];
            if (!empty($_POST['password'])) {
                $password = md5($_POST['password']);            
            } else {
                $password = null;
            }
            $sqlCorrectUser = dbConnect()->prepare("SELECT * FROM `user` WHERE `login` = :login AND `password` = :password");
            $sqlCorrectUser->execute([
                ':login' => $login,
                ':password' => $password
            ]);   
            $user = [];
            $user = $sqlCorrectUser->fetch();
            if (!empty($user)) {
                $_SESSION['user'] = $user;   
            }             
            return !empty($user);
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        }  
    }     

    /**
     * Перенаправление на другую страницу
     * @param $page
     */
    function redirect($page) 
    {
        header("Location: $page.php");
        die;
    }
    /**
     * Проверка, является ли пользователь просто авторизованным
     * @return bool
     */
    function isAuthorized()
    {
        return !empty($_SESSION['user']);   
    }

?>