 <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <link rel="stylesheet" type="text/css" href="style.css"> 
    </head>
    <body>
        <div class="container_admin"> 
            <h2><?php echo "Добро пожаловать, " . $_SESSION['user']['login'] . "!"; ?> <br> Вот Ваш список дел</h2>            
            <form method="get">
                <input name="<?= $name_insert ?>" placeholder="Введите описание дела" value="<?php if (isset($value)) echo $value['description']; ?>">  
                <input type="hidden" name="id" value="<?= $id ?>">       
                <input type="submit" name="" value="Сохранить">
            </form>    
            <form id="choice" method="get">Отсортировать по:
            </form>
                <select name="selection" form="choice">
                    <option value="date">Дате добавления</option>
                    <option value="status">Статусу</option>
                    <option value="description">Описанию</option>    
                </select>
            <input type="submit" form="choice" value="Отсортировать">
        </div>
        <table class="admin_table">
            <tr>
                <th>Описание задачи</th>
                <th>Дата добавления </th>
                <th>Статус</th>
                <th>Изменить/Выполнить/Удалить</th>   
                <th>Ответственный</th>
                <th>Автор</th>  
                <th>Закрепить задачу за пользователем</th>            
            </tr>
            <?php while ($date = $sql->fetch()) { 
                if ($date['is_done'] == 0) {
                    $status = "В процессе";
                    $color = "orange";
                } else {
                    $status = "Выполнено";
                    $color = "green";
                } ?>  
            <tr>              
                <td><?php echo $date['description'] ?></td>
                <td><?php echo $date['date_added']?></td>
                <td style="color: <?= $color?>"><?php echo $status?></td>            
                <td>
                    <a href="?id=<?= $date['id'] ?>&action=edite">Изменить</a>
                    <?php if ($date['login1'] == $date['login2']) { ?>
                    <a href="?id=<?= $date['id'] ?>&action=done">Выполнить</a>
                    <?php } ?>
                                        
                    <a href="?id=<?= $date['id'] ?>&action=delete">Удалить</a>
                    
                </td>  
                <td><?php echo $date['login2'] ?></td>
                <td><?php echo $date['login1'] ?></td>
                <td>
                    <form id="consolidate<?= $date['id'] ?>" method="get">      
                        <select name="assign_user" form="consolidate<?= $date['id'] ?>">
                            <?php 
                                foreach ($logins as $login) {
                                    foreach ($login as $key => $value) { 
                                    }                                        
                            ?>
                            <option value="<?=$login['id']?>">
                                <?php                                    
                                    echo $login['login'];
                                }
                                ?>                                
                            </option>
                        </select>
                        <input type="hidden" name="date_id" value="<?= $date['id'] ?>">
                        <input class="consolidate" type="submit" form="consolidate<?= $date['id'] ?>" value="Переложить ответственность"> 
                    </form>                       
                </td>                       
            </tr>
            <?php } ?>
        </table>
        <div class="container_admin">
            <h3>Также посмотрите, что от Вас требуют другие люди</h3>
        </div>
        <table class="admin_table">
            <tr>
                <th>Описание задачи</th>
                <th>Дата добавления </th>
                <th>Статус</th>
                <th>Изменить/Выполнить/Удалить</th>   
                <th>Ответственный</th>
                <th>Автор</th>                          
            </tr>
            <?php while ($date = $sql_require->fetch()) { 
                if ($date['is_done'] == 0) {
                    $status = "В процессе";
                    $color = "orange";
                } else {
                    $status = "Выполнено";
                    $color = "green";
                } ?>  
            <tr>              
                <td><?php echo $date['description'] ?></td>
                <td><?php echo $date['date_added']?></td>
                <td style="color: <?= $color?>"><?php echo $status?></td>            
                <td>
                    <a href="?id=<?= $date['id'] ?>&action=edite">Изменить</a>                    
                    <a href="?id=<?= $date['id'] ?>&action=done">Выполнить</a>                    
                    <?php if ($date['login1'] == $date['login2']) { ?>                    
                    <a href="?id=<?= $date['id'] ?>&action=delete">Удалить</a>
                    <?php } ?>
                </td>  
                <td><?php echo $date['login2'] ?></td>
                <td><?php echo $date['login1'] ?></td>                          
            </tr>
            <?php } ?>
        </table>
        <ul class="container_admin">
            <li><a href="logout.php" >Выйти</a></li>            
        </ul>        
    </body>
    </html>